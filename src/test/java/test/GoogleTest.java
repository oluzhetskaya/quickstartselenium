package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleTest {

    @Test
    public void testName() {

        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        By searchFieldLocator = By.name("q");
        driver.get("https://www.google.com/");

        WebElement searchField =
                new WebDriverWait(driver, 4).until(ExpectedConditions.presenceOfElementLocated(searchFieldLocator));


        searchField.sendKeys("Selenium");
        searchField.sendKeys(Keys.ENTER);

        //driver.findElement(By.name("btnK")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector(".kno-ecr-pt span")).getText(), "Selenium");

        driver.quit();
    }
}
